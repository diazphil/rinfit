 /******** This apps Stack Builder was built without any third party apps installed from this store.
 ******** Philip Diaz ===> Senior Shopify Developer/Javascript Developer/API Storefront Developer/Backend Developer/Script Editor Developer for Shopify Plus
 ******** Website: https://make8.digital
 ******** Email: philip.diaz@make8.digital | diazphi@gmail.com
 ********/
 //Size List
 $(document).on("click", ".stack-builder__head__sizes__circles__single", function() {
    var a = $(this).attr("id");
    var b = document.getElementById(a);
    var elems = document.querySelectorAll(".stack-builder__head__sizes__circles__single");
    var size = b.getAttribute("data-size");
         [].forEach.call(elems, function(el) {
             el.classList.remove("active");
         });
    b.classList.add("active"); 
    var a = $(this).attr("id");
    var b = document.getElementById(a);
    var tabs = document.querySelectorAll(".tab-item");
    var c,d;
    [].forEach.call(tabs, function(el) {
            if(el.classList.contains("active")) {
             d = el.getAttribute("data-handle");
             c = document.getElementById("stack-products-slider-" + d);
             c.querySelectorAll('.single-product-container').forEach(n => n.remove());  
             if(size === "all") {
                selected_circle_size = size;
                first_load_total_stack = 0;
                stack_total_products = 0;
                load_all_sizes(d);
             } else {
                selected_circle_size = size;
                first_load_total_stack = 0;
                stack_total_products = 0;
                ajaxcollection(d,size);
             }
            } 
         });
     });
//AJAX Collection API
function ajaxcollection(handle){
    if(handle === 'all') {
        loadingStart();
        collection_handle.forEach(function(el,index) {  
            $.getJSON({
                url: '/collections/' + el + '/products.json',
                type: 'GET',
                success: function (data) {
                    ajaxdistro(data,handle); 
                   if(Number(collection_handle.length) - 1 == index){
                    loadingStop();
                   }
                }
            });  
        });
       }else{
        loadingStart();
        $.getJSON({
            url: '/collections/' + handle + '/products.json',
            type: 'GET',
            success: function (data) {
                ajaxdistro(data,handle); 
                loadingStop();
            }
        });
       }
}
//Repopulate ajax data
function ajaxdistro(data,handle) {
    var stack_tool_tip;
    if(window.screen.availWidth < 767) {
       stack_tool_tip = "Click to add this item.";
    } else {
       stack_tool_tip = "Click to add this item.";
    }
    var c = document.getElementById("stack-products-slider-" + handle);     
    stack_total_products = 0;
    for (i = 0; i < data.products.length; i++) {
        var product = data.products[i];
                if(product.variants.length > 0) {
                    for (v = 0; v < product.variants.length; v++) {
                        var variant = product.variants[v];
                        var variant_size = variant.option1;
                        variant_size = variant_size.toLowerCase(); 
                        if (variant.compare_at_price) {
                        var variant_sale_price = Number(variant.compare_at_price) - Number(variant.price);
                        variant_sale_price = variant_sale_price.toFixed(2);
                        variant_sale_price = stack_total_zero + variant_sale_price;
                        variant_compare_at_price = stack_total_zero + variant.compare_at_price;
                        variant_regular_price = stack_total_zero + variant.price;
                        var variant_price = "<s class='product-price__price'>" + 
                                        variant_compare_at_price + "</s>" +
                                        "<span class='product-price__price product-price__sale'>" +
                                        variant_regular_price + "</span>" +
                                        "<span class='salePrice sale-tag large'>" +    
                                        "Save " + variant_sale_price + "</span>";  
                        } else {
                        var variant_price = stack_total_zero + variant.price;
                        }              
                        if(variant_size.includes("size " + size) && variant.available == true) {
                                stack_total_products = stack_total_products + 1;
                                var diva = document.createElement("div");
                                var divaa = document.createElement("div");
                                var imgab = document.createElement("img");
                                var divac = document.createElement("div");
                                var divad = document.createElement("div");
                                var divae = document.createElement("div");
                                var imgaf = document.createElement("img");
                                var divag = document.createElement("div");
                                var imgah = document.createElement("img");
                                var divai = document.createElement("div");
                                var spanaj = document.createElement("span");
                                var imgak = document.createElement("img");
                                var dival = document.createElement("div");
                                var imgam = document.createElement("img");
                                var spanan = document.createElement("span");
                                var divao = document.createElement("div");
                                var divap = document.createElement("div");
                                var spanaq = document.createElement("span");
                                var spanar = document.createElement("span");
                                var spanas = document.createElement("span");
                                var spanat = document.createElement("span");   
                                var spanau = document.createElement("span");  
                                var divav = document.createElement("div");       
                                var checkboxaw = document.createElement("input");      
                                var labelax = document.createElement("label");                    
                                diva.setAttribute("data-id", variant.id);
                                diva.classList.add("single-product-container");
                                diva.setAttribute("id", "product-" + variant.id);
                                diva.setAttribute("data-size", variant.title);
                                diva.setAttribute("style", "border-color: transparent;");
                                diva.appendChild(divaa);
                                divaa.classList.add("single-product");
                                divaa.appendChild(imgab);
                                imgab.setAttribute("src", variant.featured_image.src);
                                imgab.classList.add("main-image");
                                divaa.appendChild(divac);
                                divac.classList.add("product-info");
                                divac.setAttribute("id","selected-parent-" + variant.id);
                                divac.appendChild(divad);
                                divad.classList.add("droppable-container");
                                divad.classList.add("has-item");
                                divad.setAttribute("id", "div-" + variant.id);
                                divad.setAttribute("data-id", variant.id);
                                divad.setAttribute("data-price", variant.price);
                                divad.setAttribute("style", "display: none");
                                divad.appendChild(divae);
                                divae.classList.add("item-reorder");
                                divae.appendChild(imgaf);
                                imgaf.setAttribute("src",stack_arrow_down);
                                imgaf.setAttribute("onclick","itemmovedown(this)");
                                divae.appendChild(spanaj);
                                spanaj.innerHTML = "REORDER";
                                divae.appendChild(imgah);
                                imgah.setAttribute("src",stack_arrow_up);
                                imgah.setAttribute("onclick","itemmoveup(this)");
                                divad.appendChild(divai);
                                divai.classList.add("ring-stack");
                                divai.classList.add("droppable");
                                divai.appendChild(divag);
                                divag.classList.add("single-product");
                                divag.appendChild(imgak);
                                imgak.setAttribute("src", variant.featured_image.src);
                                imgak.setAttribute("alt","stack image");
                                imgak.classList.add("alt-image");
                                divad.appendChild(dival);
                                dival.classList.add("item-delete");
                                dival.setAttribute("stack-selected", "selected-" + variant.id);
                                dival.setAttribute("stack-selected-label", "selected-label-" + variant.id);
                                dival.setAttribute("stack-selected-parent", "selected-parent-" + variant.id);
                                dival.appendChild(imgam);
                                imgam.setAttribute("src",stack_delete_icon);
                                dival.appendChild(spanan);
                                spanan.innerHTML = "DELETE";
                                divac.appendChild(divao);
                                divao.classList.add("droppable-container");
                                divao.classList.add("has-no-item");
                                divao.setAttribute("style","display: none;margin-top: 8px;");
                                divao.appendChild(divap);
                                divap.classList.add("ring-stack");
                                divap.classList.add("droppable");
                                divap.appendChild(spanaq);
                                spanaq.classList.add("droppable-placeholder");
                                spanaq.innerHTML = "ADD RING";
                                divac.appendChild(divav);
                                divav.classList.add("product_info_details");
                                divav.appendChild(checkboxaw);
                                checkboxaw.setAttribute("type", "checkbox");
                                checkboxaw.setAttribute("id", "selected-" + variant.id);
                                checkboxaw.setAttribute("disabled", true);
                                checkboxaw.setAttribute("name","selected_item");
                                checkboxaw.checked = false;
                                divav.appendChild(labelax);
                                labelax.setAttribute("id","selected-label-" + variant.id);
                                labelax.setAttribute("for","selected-" + variant.id);
                                labelax.setAttribute("name","selected_item");
                                labelax.innerHTML ="This item [Not added yet]";
                                divac.appendChild(spanar);
                                spanar.classList.add("product-title");
                                spanar.innerHTML = variant.title;   
                                divac.appendChild(spanas);
                                spanas.innerHTML = product.product_type;
                                divac.appendChild(spanat);
                                spanat.setAttribute("flow-variant","prices.item.label");
                                spanat.setAttribute("flow-default", stack_total_zero + variant.price);
                                spanat.classList.add("flow-localized");
                                spanat.innerHTML = variant_price;
                                divac.appendChild(spanau);
                                divac.classList.add("tooltip");
                                spanau.innerHTML = stack_tool_tip;
                                spanau.classList.add("tooltiptext");
                                if(v === 0) {
                                c.appendChild(diva);
                                } else {
                                $(diva).insertAfter(c.lastChild);
                                }              
                        }
                }
        }    
}
}
    // Tab List
 $(document).on("click", ".tab-item", function() {
    var a = $(this).attr("id");
    var b = document.getElementById(a);
    var c = b.getAttribute("data-handle");
    var d = document.getElementById("stack-products-slider-" + c);
    var elems = document.querySelectorAll(".tab-item");
         [].forEach.call(elems, function(el) {
             el.classList.remove("active");
         });
    var tab = document.querySelectorAll(".stack-products-slider");
         [].forEach.call(tab, function(el) {
            el.style.display = "none";
          });
    b.classList.add("active"); 
    d.style.display = "flex";
});
// Product Add to Stack
$(document).on("click", ".add-ring-to-left", function() {
var a = document.getElementById("dropzoneitem");
var b = document.createElement("div");
var c = document.createElement("div");
var d = document.createElement("div");
var e = document.createElement("span");
var f = document.createElement("div");
var img = document.createElement("img");
var noitem = document.querySelectorAll("#dropzoneitem .has-no-item");
var withitem = document.querySelectorAll("#dropzoneitem .has-item");
var variant_selected = document.querySelectorAll(".product-variants-color-select-" + this.getAttribute("data-id"));
console.log(variant_selected.length);
if(noitem.length == 2) {
a.innerHTML = '';
b.classList.add("droppable-container");
b.classList.add("has-item");
b.setAttribute("variant-id", this.getAttribute("data-id"))
img.src = this.getAttribute("data-color");
f.classList.add("ring-stack");
f.classList.add("droppable");
f.appendChild(img);
b.appendChild(f);
c.classList.add("droppable-container");
c.classList.add("has-no-item");
d.classList.add("ring-stack");
d.classList.add("droppable");
c.appendChild(d);
e.classList.add("droppable-placeholder");
e.innerHTML = "ADD RING";
d.appendChild(e);
a.appendChild(b);
a.appendChild(c);
}else {
[].forEach.call(noitem, function(el) {
    if (el.classList.contains("has-no-item")){
        a.removeChild(a.lastChild);
    }
});
b.classList.add("droppable-container");
b.classList.add("has-item");
img.src = this.getAttribute("data-color");
f.classList.add("ring-stack");
f.classList.add("droppable");
f.appendChild(img);
b.appendChild(f);
a.appendChild(b);
}          
});
// Add Ring Tool Tips 
$(document).on("click", ".droppable-placeholder", function(){
const icon = document.querySelector(".icon-arrow");
const arrow = document.querySelector(".tiparrow");
const texttip = document.getElementById("tool_tips_picker");
icon.style.display = "block";
texttip.style.display = "block";
        arrow.animate([
          {left: '0'},
          {left: '10px'},
          {left: '0'}
        ],{
          duration: 800,
          iterations: Infinity
        });
})
// Add Ring Tool Tips 
$(document).on("click", ".single-product", function(){
    const icon = document.querySelector(".icon-arrow");
    const texttip = document.getElementById("tool_tips_picker");
    icon.style.display = "none";
    texttip.style.display = "none";
    })
// Disappering Tool Tips
$(document).on("click", ".icon-arrow", function(){
    a = document.getElementById("icon-arrow");
    const texttip = document.getElementById("tool_tips_picker");
    setTimeout(() => {
        a.style.display = "none";
        texttip.style.display = "none";
    }, 888);
})
// Disappering Tool Text Tips
$(document).on("click", ".tool_tips_picker", function(){
    a = document.getElementById("icon-arrow");
    const texttip = document.getElementById("tool_tips_picker");
    setTimeout(() => {
        a.style.display = "none";
        texttip.style.display = "none";
    }, 888);
})
// Item Delete
$(document).on("click", ".item-delete", function() { 
    var itemcount = 0;
    var a = document.getElementById("dropzoneitem");
    var b = document.getElementById("stack-button-to-cart");
    var clonehasnoitem = document.getElementById("hasnoitemstacktocart");
    var cloneiflessthantwo = clonehasnoitem.cloneNode(true);
    this.parentElement.remove();
    var total = 0;
    var totalcart = document.getElementById("totalcartamount");
    var d = document.getElementById(this.getAttribute("stack-selected"));
    var e = document.getElementById(this.getAttribute("stack-selected-parent"));
    var f = document.getElementById(this.getAttribute("stack-selected-label"));
    d.checked = false;
    e.style.backgroundColor = "#fff";
    f.innerHTML = "This item [Not added yet.].";
    var elems = document.querySelectorAll(".stacktocart");
         [].forEach.call(elems, function(el) {
             total = total + Number(el.getAttribute("data-price"));                 
         });
    totalcart.innerHTML = "Total: " + stack_total_zero + total.toFixed(2);   
    var items = document.querySelectorAll(".stacktocart");
    [].forEach.call(items, function(el) {
        itemcount = itemcount + 1;
    });
    if(itemcount > 0) {
                b.classList.add("enabled");
                b.classList.remove("disabled");
                b.innerHTML = "ADD STACK(" + itemcount + " items) TO CART";
                b.disabled = false;
    } else {
                a.appendChild(cloneiflessthantwo);
                b.classList.add("disabled");
                b.classList.remove("enabled");
                b.innerHTML = "ADD STACK(0 item) TO CART";
                b.disabled = true;
    }      
});
// Item move down
function itemmovedown(el) {
    var parentid = el.parentNode.parentNode.getAttribute("id");
    var refnode = document.getElementById(parentid);
    var lastsiblingid = refnode.nextSibling.getAttribute("id");
    var lastsibling = document.getElementById(lastsiblingid);
    var hasnoitem = lastsibling.classList.contains("has-no-item");
    if(hasnoitem == false) {
    var nextsibling = document.getElementById(refnode.nextSibling.getAttribute("id"));
    $(refnode).insertAfter(nextsibling);      
    }
}
// Item move up
function itemmoveup(el) {
    var parentid = el.parentNode.parentNode.getAttribute("id");
    var refnode = document.getElementById(parentid);
    var grandparentid = refnode.parentNode.getAttribute("id");
    var grandparent = document.getElementById(grandparentid);
    var firstchildid = grandparent.firstElementChild.getAttribute("id");
    var firstchild = document.getElementById(firstchildid);
    if(firstchild !== refnode) {
     var prevsiblingid = refnode.previousSibling.getAttribute("id");
     var prevsibling = document.getElementById(prevsiblingid);
     $(refnode).insertBefore(prevsibling);
    } 
 }
 //Selected stack item to cart
    function additemtocart(b){
        var itemObj;
        var items = document.querySelectorAll(".stacktocart");
        [].forEach.call(items, function(el,index) {
            if(index === 0) {
                itemObj = [el.getAttribute("data-id")];
            } else {
                itemObj.push(el.getAttribute("data-id"));
            }
                
        });
        Shopify.queue = [];
          itemObj.forEach( function(item) {
            Shopify.queue.push({
              variantId: item,
            });
          });
          Shopify.moveAlong = function() {
          // If we still have requests in the queue, let's process the next one.
          if (Shopify.queue.length) {
            var request = Shopify.queue.shift();
            var data = 'id='+ request.variantId + '&quantity=1'
            $.ajax({
              type: 'POST',
                  url: '/cart/add.js',
              dataType: 'json',
              data: data,
              success: function(){
                Shopify.moveAlong();
             },
                 error: function(){
             // if it's not last one Move Along else update the cart number with the current quantity
              if (Shopify.queue.length){
                Shopify.moveAlong()
              }
              }
               });
            }
           };
        Shopify.moveAlong();
        b.innerHTML = "Your stack successfully added to cart";
        setTimeout(function(){b.innerHTML = "Thank you!"}, 800);
        setTimeout("location.href = '/cart';",2800);
      }
      //display all size and collections
      function load_all_sizes(h){
        for (r = Number(load_min_size); r <= Number(load_max_size); r++) {  
          ajaxcollection(h);  
        }
      }
//horizontal scroll for tab title
var btnnext = document.getElementById('next-arrow');
btnnext.onclick = function () {
    var container = document.getElementById('stackables-tabs');
    sideScroll(container,'right',25,100,10);
};
var btnprev = document.getElementById('prev-arrow');
btnprev.onclick = function () {
    var container = document.getElementById('stackables-tabs');
    sideScroll(container,'left',25,100,10);
};
function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

$(document).on("click",".stack-builder-option-women-size", function(){
    var menid = document.getElementById("stack-builder-popup-size-" + this.getAttribute("data-id"));
    var womenid = document.getElementById("stack-builder-popup-size-women-" + this.getAttribute("data-id"));
    var act = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    var actmen = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    if(womenid){womenid.classList.remove("d-none");}
    if(menid){menid.classList.add("d-none");}
    [].forEach.call(act, function(el){
        if(women_size_selected == ""){
            el.classList.remove("active");
        } 
    });
    if(men_size_selected == "" && actmen.length>0){
        actmen[0].classList.add("active");
        actmen[0].style.backgroundColor = "white", "important";
    }else{
        if(actmen>0){actmen[0].removeAttribute("style");}
    };
    });
    $(document).on("click",".stack-builder-option-men-size", function(){
    var menid = document.getElementById("stack-builder-popup-size-" + this.getAttribute("data-id"));
    var womenid = document.getElementById("stack-builder-popup-size-women-" + this.getAttribute("data-id"));
    var act = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var actwomen = document.querySelectorAll(".ring-size-women-active" + this.getAttribute("data-id"));
    if(menid){menid.classList.remove("d-none");}
    if(womenid){womenid.classList.add("d-none");}
       [].forEach.call(act, function(el){
        if(el.classList.contains("active") && men_size_selected == ""){
            el.classList.remove("active");
        }
    });
    
    if(women_size_selected == "" && actwomen.length>0){
        actwomen[0].classList.add("active");
        actwomen[0].style.backgroundColor = "white", "important";
    }else{
        if(actwomen>0){actwomen[0].removeAttribute("style");}
    };
    });
    $(document).on("click",".stack-builder-product-button-add-to-cart", function(){
        men_size_selected = "";
        women_size_selected = "";
        var size = document.getElementById("stack-builder-selected_size_men_" + this.getAttribute("data-id"));
        var sizew = document.getElementById("stack-builder-selected_size_women_" + this.getAttribute("data-id"));
        var comsize = document.getElementById("stack-builder-selected_size_" + this.getAttribute("data-id"));
        var id = document.getElementById("stack-builder-popup-size-" + this.getAttribute("data-id"));
        var btn = document.getElementById("stack-builder-product-btn-" + this.getAttribute("data-id"));
        var itm = document.getElementById("stack-builder-color-image-clone--" + this.getAttribute("data-id"));
        var itmw = document.getElementById("stack-builder-color-image-women-clone--" + this.getAttribute("data-id"));
        var act = document.querySelectorAll(".product-options__value--circle--" + this.getAttribute("data-id"));
        var color = this.getAttribute("data-selected-color");
        var btnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
        var btnfinalcart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
        var btnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
        var btnwomenfinalcart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
        var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
        var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
        if(comsize)(comsize.innerHTML = "Size");
        if(size){size.innerHTML = "Size"} 
        if(sizew){sizew.innerHTML = "Size"}
        if (sizeactive.length>0) {
                        [].forEach.call(sizeactive,function(el){
                            if(el.classList.contains("active")){
                                el.classList.remove("active");
                            }
                        })
                    }
        if (sizewomenactive.length>0) {
                       [].forEach.call(sizewomenactive,function(el){
                            if(el.classList.contains("active")){
                                el.classList.remove("active");
                            }
                        })
                    }
    for (i = 0; i < act.length; i++) {
            if(act[i].classList.contains("active")){
                if(color == act[i].getAttribute("data-value")){
                    var a = document.createElement("div");
                    if(itm.children.length>0) {                      
                    itm.removeChild(itm.children[0]);  
                    }    
                    a.classList.add("product-option-color-selected");
                    a.style.backgroundImage = act[i].getAttribute("data-bg");
                    itm.appendChild(a);
                }else if(color == null){
                    if(itm.children.length>0) {                      
                    itm.removeChild(itm.children[0]);  
                    }             
                    var a = document.createElement("div");
                    a.classList.add("product-option-color-selected");
                    a.style.backgroundImage = act[i].getAttribute("data-bg");
                    itm.appendChild(a);  
                }              
            }
        }
        if(act.length <= 3){
            if(itm && itm.children.length>0) {
                    itm.removeChild(itm.children[0]);  
                    var mendiv = document.createElement("div");
                    mendiv.classList.add("product-option-color-selected");      
                    mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                    itm.appendChild(mendiv);               
                } else {
                    var mendiv = document.createElement("div");
                    mendiv.classList.add("product-option-color-selected");      
                    mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                    if(itm){itm.appendChild(mendiv);}                           
            }
            if(itmw && itmw.children.length>0) {
                itmw.removeChild(itmw.children[0]);  
                var womendiv = document.createElement("div");
                womendiv.classList.add("product-option-color-selected");      
                womendiv.style.backgroundImage = act[0].style.backgroundImage;      
                itmw.appendChild(mendiv);                  
            } else {
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;    
                if(itmw){itmw.appendChild(mendiv);}                     
            }      
    }  
    if(btnring){
    btnring.classList.remove("d-none");
    btnfinalcart.classList.add("d-none");
    }
    if(btnwomenring){
    btnwomenring.classList.remove("d-none");
    btnwomenfinalcart.classList.add("d-none");
    }
    if(id){id.classList.toggle("d-none");}
    if(btn){btn.classList.toggle("d-none");}
    this.classList.toggle("d-none");
    });
    $(document).on("click",".stack-builder-product-options__value--text", function(){
        var id = document.getElementById("stack-builder-product-size-label-" + this.getAttribute("data-id"));
        var label = document.getElementById("stack-builder-sizedroplabel-"  + this.getAttribute("data-id"));
        var size = document.getElementById("stack-builder-selected_size_men_" + this.getAttribute("data-id"));
        var sizew = document.getElementById("stack-builder-selected_size_women_" + this.getAttribute("data-id"));
        var sizesv = document.getElementById("stack-builder-selected_size_" + this.getAttribute("data-id"));
        var btnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
        var btnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
        var btncart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
        var btnwomencart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
        var act = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
        var actmen = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
        men_size_selected = this.getAttribute("data-value");
        page_selected_size1 = this.getAttribute("data-value");
        var btnpage = document.getElementById("stack-builder-buycartpage-" + this.getAttribute("data-id"));
        var btnpagecart = document.getElementById("stack-builder-addcartpage-" + this.getAttribute("data-id"));
        var available;
        if(this.classList.contains("disabled")) {
            available = false;
            if(btnring){btnring.classList.remove("d-none");}
            if(btncart){btncart.classList.add("d-none");}
            if(btnwomenring){btnwomenring.classList.remove("d-none");}
            if(btnwomencart){btnwomencart.classList.add("d-none");}
        } else {
            available = true;
        }
        if(id){id.innerHTML = "Size " + this.innerHTML;}
        if(sizesv){sizesv.innerHTML = "Size " + this.innerHTML;}
        if(label){label.innerHTML = "Size " + this.innerHTML;}
        if(btncart && btncart.classList.contains("d-none") && act.length==0 && available){
            if(btnring){btnring.classList.add("d-none");}
            if(btncart){btncart.classList.remove("d-none");}
        }
        if(btncart && btncart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
            if(btnring){btnring.classList.add("d-none");}
            if(btncart){btncart.classList.remove("d-none");}
        }
        if(btnwomencart && btnwomencart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
            if(btnwomenring){btnwomenring.classList.add("d-none");}
            if(btnwomencart){btnwomencart.classList.remove("d-none");}
        }
        if(women_size_selected === ""){
            if(btnpage) {btnpage.innerHTML = "Add to cart";}
            if(act.length>0){act[0].style.backgroundColor = "white", "important";}
        }else{
            if(act.length>0){act[0].removeAttribute("style");}
        }
        if(actmen.length>0){
            actmen[0].removeAttribute("style");
        }
        if(size && men_size_selected) {
            size.innerHTML = "Size " + men_size_selected.replace("Size","") + "/";
            sizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/";
        }
        if(sizew && women_size_selected && men_size_selected) {
            size.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
            sizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
        }try {
            if(page_selected_size1 !== "" && page_selected_size2 !== ""){
                if(bnpage && btnpage.classList.contains("d-none") == false) {
                    btnpage.classList.add("d-none");
                    if(btnpagecart){btnpagecart.classList.remove("d-none");}
                  }       
            } 
        } catch(err){ return;}  
        [].forEach.call(act, function(el){
            if(women_size_selected == ""){
                el.classList.remove("active");
            } 
        });
        });
        $(document).on("click",".stack-builder-product-options__value--text--women", function(){
            var id = document.getElementById("stack-builder-product-size-label-women-" + this.getAttribute("data-id"));
            var label = document.getElementById("stack-builder-sizedroplabelwomen-" + this.getAttribute("data-id"));
            var btnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
            var btnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
            var btncart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
            var btnwomencart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
            var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
            var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
            var sizew = document.getElementById("stack-builder-selected_size_women_" + this.getAttribute("data-id"));
            var size = document.getElementById("stack-builder-selected_size_men_" + this.getAttribute("data-id"));
            page_selected_size2 = this.getAttribute("data-value");
            var btnpage = document.getElementById("stack-builder-buycartpage-" + this.getAttribute("data-id"));
            var btnpagecart = document.getElementById("stack-builder-addcartpage-" + this.getAttribute("data-id"));
            women_size_selected = this.getAttribute("data-value");
            if (sizewomenactive.length>0) {
                [].forEach.call(sizewomenactive,function(el){
                        el.classList.remove("active");
                })
            };
            if(sizewomenactive.length>0){sizewomenactive[0].removeAttribute("style");}
            if(men_size_selected === ""){
                if(btnpage) {
                    btnpage.innerHTML = "Add to cart";
                    btnpage.classList.add("half_disabled_add_to_cart");
                }
                if(sizeactive.length>0){
                    sizeactive[0].style.backgroundColor = "white", "important";
                }
            };
            this.classList.add("active");
            if(id){id.innerHTML = "Size " + this.innerHTML;}
            if(label){label.innerHTML = "Size " + this.innerHTML;}
            if(btncart && btncart.classList.contains("d-none") && men_size_selected !== ""){
                btnring.classList.toggle("d-none");
                btncart.classList.toggle("d-none");
            }
            if(btnwomencart && btnwomencart.classList.contains("d-none") && men_size_selected !== ""){
                btnwomenring.classList.toggle("d-none");
                btnwomencart.classList.toggle("d-none");
            }
            if(sizew && women_size_selected) {
                sizew.innerHTML = "Size " + "/" + women_size_selected.replace("Size","");
                size.innerHTML = "Size " + "/" + women_size_selected.replace("Size","");
            }
            if(sizew && men_size_selected && women_size_selected) {
                sizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
                size.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
            }
            try {
                if(page_selected_size1 !== "" && page_selected_size2 !== ""){
                    if(btnpage && btnpage.classList.contains("d-none") == false) {
                        btnpage.classList.toggle("d-none");
                        btnpagecart.classList.toggle("d-none");
                      }     
                } 
            } catch(err){ return;}  
            });
            $(document).on("click",".stack-builder-product-options__value--circle", function(){
                btn = document.getElementById("btn-product-cart-id-" + this.getAttribute("data-id"));
                sbbtn = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
                imgsrc = this.style.backgroundImage;
                imgsrc = imgsrc.replace('url("','');
                imgsrc = imgsrc.replace('")','');
                if(btn){
                btn.setAttribute("data-selected-color",this.getAttribute("data-value"));
                }   
                if(sbbtn){
                sbbtn.setAttribute("data-color",imgsrc);
                }   
            });