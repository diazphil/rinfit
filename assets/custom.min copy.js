 /******** This script is for the popup size selector and upsell pop-up cart. Please don't removed
 ******** Author: Philip Diaz ===> Senior Shopify Developer/Javascript Developer/API Storefront Developer
 ******** Website: https://make8.digital
 ******** Email: philip.diaz@make8.digital | diazphi@gmail.com
 ********/
 //Size List
var men_size_selected = "", women_size_selected = "",variant_color = "", variant_size = "", page_selected_color = "", page_selected_size = "";
$(document).on("click",".product-button-add-to-cart", function(){
    men_size_selected = "";
    women_size_selected = "";
    var size = document.getElementById("selected_size_men_" + this.getAttribute("data-id"));
    var sizew = document.getElementById("selected_size_women_" + this.getAttribute("data-id"));
    var comsize = document.getElementById("selected_size_" + this.getAttribute("data-id"));
    var id = document.getElementById("popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("product-btn-" + this.getAttribute("data-id"));
    var itm = document.getElementById("color-image-clone--" + this.getAttribute("data-id"));
    var itmw = document.getElementById("color-image-women-clone--" + this.getAttribute("data-id"));
    var idel = this.getAttribute("data-id");
    var act = document.querySelectorAll(".product-options__value--circle--" + this.getAttribute("data-id"));
    var color = this.getAttribute("data-selected-color");
    var btnring = document.getElementById("buyringsize-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("buyringsize-women-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if(comsize)(comsize.innerHTML = "Size");
    if(size){size.innerHTML = "Size"} 
    if(sizew){sizew.innerHTML = "Size"}
    if (sizeactive.length>0) {
                    [].forEach.call(sizeactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
    if (sizewomenactive.length>0) {
                   [].forEach.call(sizewomenactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
for (i = 0; i < act.length; i++) {
        if(act[i].classList.contains("active")){
            if(color == act[i].getAttribute("data-value")){
                var a = document.createElement("div");
                if(itm.children.length>0) {                      
                itm.removeChild(itm.children[0]);  
                }    
                a.classList.add("product-option-color-selected");
                a.style.backgroundImage = act[i].getAttribute("data-bg");
                itm.appendChild(a);
            }else if(color == null){
                if(itm.children.length>0) {                      
                itm.removeChild(itm.children[0]);  
                }             
                var a = document.createElement("div");
                a.classList.add("product-option-color-selected");
                a.style.backgroundImage = act[i].getAttribute("data-bg");
                itm.appendChild(a);  
            }              
        }
    }
    if(act.length <= 3){
        if(itm && itm.children.length>0) {
                itm.removeChild(itm.children[0]);  
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                itm.appendChild(mendiv);               
            } else {
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                if(itm){itm.appendChild(mendiv);}                           
        }
        if(itmw && itmw.children.length>0) {
            itmw.removeChild(itmw.children[0]);  
            var womendiv = document.createElement("div");
            womendiv.classList.add("product-option-color-selected");      
            womendiv.style.backgroundImage = act[0].style.backgroundImage;      
            itmw.appendChild(mendiv);                  
        } else {
            var mendiv = document.createElement("div");
            mendiv.classList.add("product-option-color-selected");      
            mendiv.style.backgroundImage = act[0].style.backgroundImage;    
            if(itmw){itmw.appendChild(mendiv);}                     
        }      
}  
if(btnring){
btnring.classList.remove("d-none");
btnfinalcart.classList.add("d-none");
}
if(btnwomenring){
btnwomenring.classList.remove("d-none");
btnwomenfinalcart.classList.add("d-none");
}
if(id){id.classList.toggle("d-none");}
if(btn){btn.classList.toggle("d-none");}
this.classList.toggle("d-none");
});
$(document).on("click",".upsell-product-button-add-to-cart", function(){
    men_size_selected = "";
    women_size_selected = "";
    var size = document.getElementById("upsell-selected_size_men_" + this.getAttribute("data-id"));
    var sizew = document.getElementById("upsell-selected_size_women_" + this.getAttribute("data-id"));
    var comsize = document.getElementById("upsell-selected_size_" + this.getAttribute("data-id"));
    var id = document.getElementById("upsell-popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("upsell-product-btn-" + this.getAttribute("data-id"));
    var itm = document.getElementById("upsell-color-image-clone--" + this.getAttribute("data-id"));
    var itmw = document.getElementById("upsell-color-image-women-clone--" + this.getAttribute("data-id"));
    var act = document.querySelectorAll(".product-options__value--circle--" + this.getAttribute("data-id"));
    var color = this.getAttribute("data-selected-color");
    var btnring = document.getElementById("upsell-buyringsize-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("upsell-finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("upsell-buyringsize-women-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("upsell-finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if(comsize)(comsize.innerHTML = "Size");
    if(size){size.innerHTML = "Size"} 
    if(sizew){sizew.innerHTML = "Size"}
    if (sizeactive.length>0) {
                    [].forEach.call(sizeactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
    if (sizewomenactive.length>0) {
                   [].forEach.call(sizewomenactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
for (i = 0; i < act.length; i++) {
        if(act[i].classList.contains("active")){
            if(color == act[i].getAttribute("data-value")){
                var a = document.createElement("div");
                if(itm.children.length>0) {                      
                itm.removeChild(itm.children[0]);  
                }    
                a.classList.add("product-option-color-selected");
                a.style.backgroundImage = act[i].getAttribute("data-bg");
                itm.appendChild(a);
            }else if(color == null){
                if(itm.children.length>0) {                      
                itm.removeChild(itm.children[0]);  
                }             
                var a = document.createElement("div");
                a.classList.add("product-option-color-selected");
                a.style.backgroundImage = act[i].getAttribute("data-bg");
                itm.appendChild(a);  
            }              
        }
    }
    if(act.length <= 3){
        if(itm && itm.children.length>0) {
                itm.removeChild(itm.children[0]);  
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                itm.appendChild(mendiv);               
            } else {
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                if(itm){itm.appendChild(mendiv);}                           
        }
        if(itmw && itmw.children.length>0) {
            itmw.removeChild(itmw.children[0]);  
            var womendiv = document.createElement("div");
            womendiv.classList.add("product-option-color-selected");      
            womendiv.style.backgroundImage = act[0].style.backgroundImage;      
            itmw.appendChild(mendiv);                  
        } else {
            var mendiv = document.createElement("div");
            mendiv.classList.add("product-option-color-selected");      
            mendiv.style.backgroundImage = act[0].style.backgroundImage;    
            if(itmw){itmw.appendChild(mendiv);}                     
        }      
}  
if(btnring){
btnring.classList.remove("d-none");
btnfinalcart.classList.add("d-none");
}
if(btnwomenring){
btnwomenring.classList.remove("d-none");
btnwomenfinalcart.classList.add("d-none");
}
if(id){id.classList.toggle("d-none");}
if(btn){btn.classList.toggle("d-none");}
this.classList.toggle("d-none");
});
$(document).on("click",".stack-builder-product-button-add-to-cart", function(){
    men_size_selected = "";
    women_size_selected = "";
    var size = document.getElementById("stack-builder-selected_size_men_" + this.getAttribute("data-id"));
    var sizew = document.getElementById("stack-builder-selected_size_women_" + this.getAttribute("data-id"));
    var comsize = document.getElementById("stack-builder-selected_size_" + this.getAttribute("data-id"));
    var id = document.getElementById("stack-builder-popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("stack-builder-product-btn-" + this.getAttribute("data-id"));
    var itm = document.getElementById("stack-builder-color-image-clone--" + this.getAttribute("data-id"));
    var itmw = document.getElementById("stack-builder-color-image-women-clone--" + this.getAttribute("data-id"));
    var act = document.querySelectorAll(".product-options__value--circle--" + this.getAttribute("data-id"));
    var color = this.getAttribute("data-selected-color");
    var btnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if(comsize)(comsize.innerHTML = "Size");
    if(size){size.innerHTML = "Size"} 
    if(sizew){sizew.innerHTML = "Size"}
    if (sizeactive.length>0) {
                    [].forEach.call(sizeactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
    if (sizewomenactive.length>0) {
                   [].forEach.call(sizewomenactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
for (i = 0; i < act.length; i++) {
        if(act[i].classList.contains("active")){
            if(color == act[i].getAttribute("data-value")){
                var a = document.createElement("div");
                if(itm.children.length>0) {                      
                itm.removeChild(itm.children[0]);  
                }    
                a.classList.add("product-option-color-selected");
                a.style.backgroundImage = act[i].getAttribute("data-bg");
                itm.appendChild(a);
            }else if(color == null){
                if(itm.children.length>0) {                      
                itm.removeChild(itm.children[0]);  
                }             
                var a = document.createElement("div");
                a.classList.add("product-option-color-selected");
                a.style.backgroundImage = act[i].getAttribute("data-bg");
                itm.appendChild(a);  
            }              
        }
    }
    if(act.length <= 3){
        if(itm && itm.children.length>0) {
                itm.removeChild(itm.children[0]);  
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                itm.appendChild(mendiv);               
            } else {
                var mendiv = document.createElement("div");
                mendiv.classList.add("product-option-color-selected");      
                mendiv.style.backgroundImage = act[0].style.backgroundImage;      
                if(itm){itm.appendChild(mendiv);}                           
        }
        if(itmw && itmw.children.length>0) {
            itmw.removeChild(itmw.children[0]);  
            var womendiv = document.createElement("div");
            womendiv.classList.add("product-option-color-selected");      
            womendiv.style.backgroundImage = act[0].style.backgroundImage;      
            itmw.appendChild(mendiv);                  
        } else {
            var mendiv = document.createElement("div");
            mendiv.classList.add("product-option-color-selected");      
            mendiv.style.backgroundImage = act[0].style.backgroundImage;    
            if(itmw){itmw.appendChild(mendiv);}                     
        }      
}  
if(btnring){
btnring.classList.remove("d-none");
btnfinalcart.classList.add("d-none");
}
if(btnwomenring){
btnwomenring.classList.remove("d-none");
btnwomenfinalcart.classList.add("d-none");
}
if(id){id.classList.toggle("d-none");}
if(btn){btn.classList.toggle("d-none");}
this.classList.toggle("d-none");
});
$(document).on("click",".close-overlay", function(){
    men_size_selected = "";
    women_size_selected = "";
    var id = document.getElementById("popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("product-btn-" + this.getAttribute("data-id"));
    var btncart = document.getElementById("btn-product-cart-id-" + this.getAttribute("data-id"));
    var idwomen = document.getElementById("popup-size-women-" + this.getAttribute("data-id"));
    var btnring = document.getElementById("buyringsize-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("buyringsize-women-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if (sizeactive.length>0) {
                    [].forEach.call(sizeactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
    if (sizewomenactive.length>0) {
                    [].forEach.call(sizewomenactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }            
    if(sizeactive[0]){sizeactive[0].classList.add("active");}
    if(sizewomenactive[0]){sizewomenactive[0].classList.add("active");}
    if(btnring){
        btnring.classList.remove("d-none");
        btnfinalcart.classList.add("d-none");
    }
    if(btnwomenring){
        btnwomenring.classList.remove("d-none");
        btnwomenfinalcart.classList.add("d-none");
    }
if(id){id.classList.add("d-none");}
if(btn){btn.classList.add("d-none");}
if(btncart){btncart.classList.remove("d-none");}
if(idwomen){idwomen.classList.add("d-none");}
});
$(document).on("click",".upsell-close-overlay", function(){
    men_size_selected = "";
    women_size_selected = "";
    var id = document.getElementById("upsell-popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("upsell-product-btn-" + this.getAttribute("data-id"));
    var btncart = document.getElementById("upsell-btn-product-cart-id-" + this.getAttribute("data-id"));
    var idwomen = document.getElementById("upsell-popup-size-women-" + this.getAttribute("data-id"));
    var btnring = document.getElementById("upsell-buyringsize-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("upsell-buyringsize-women-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("upsell-finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("upsell-finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if (sizeactive.length>0) {
                    [].forEach.call(sizeactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
    if (sizewomenactive.length>0) {
                    [].forEach.call(sizewomenactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }            
    if(sizeactive[0]){sizeactive[0].classList.add("active");}
    if(sizewomenactive[0]){sizewomenactive[0].classList.add("active");}
    if(btnring){
        btnring.classList.remove("d-none");
        btnfinalcart.classList.add("d-none");
    }
    if(btnwomenring){
        btnwomenring.classList.remove("d-none");
        btnwomenfinalcart.classList.add("d-none");
    }
if(id){id.classList.add("d-none");}
if(btn){btn.classList.add("d-none");}
if(btncart){btncart.classList.remove("d-none");}
if(idwomen){idwomen.classList.add("d-none");}
});
$(document).on("click",".stack-builder-close-overlay", function(){
    men_size_selected = "";
    women_size_selected = "";
    var id = document.getElementById("stack-builder-popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("stack-builder-product-btn-" + this.getAttribute("data-id"));
    var btncart = document.getElementById("stack-builder-btn-product-cart-id-" + this.getAttribute("data-id"));
    var idwomen = document.getElementById("stack-builder-popup-size-women-" + this.getAttribute("data-id"));
    var btnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if (sizeactive.length>0) {
                    [].forEach.call(sizeactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }
    if (sizewomenactive.length>0) {
                    [].forEach.call(sizewomenactive,function(el){
                        if(el.classList.contains("active")){
                            el.classList.remove("active");
                        }
                    })
                }            
    if(sizeactive[0]){sizeactive[0].classList.add("active");}
    if(sizewomenactive[0]){sizewomenactive[0].classList.add("active");}
    if(btnring){
        btnring.classList.remove("d-none");
        btnfinalcart.classList.add("d-none");
    }
    if(btnwomenring){
        btnwomenring.classList.remove("d-none");
        btnwomenfinalcart.classList.add("d-none");
    }
if(id){id.classList.add("d-none");}
if(btn){btn.classList.add("d-none");}
if(btncart){btncart.classList.remove("d-none");}
if(idwomen){idwomen.classList.add("d-none");}
});
$(document).on("click",".upsell-close-overlay-women", function(){
    men_size_selected = "";
    women_size_selected = "";
    var id = document.getElementById("upsell-popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("upsell-product-btn-" + this.getAttribute("data-id"));
    var btncart = document.getElementById("upsell-btn-product-cart-id-" + this.getAttribute("data-id"));
    var idwomen = document.getElementById("upsell-popup-size-women-" + this.getAttribute("data-id"));
    var btnring = document.getElementById("upsell-buyringsize-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("upsell-buyringsize-women-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("upsell-finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("upsell-finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if (sizeactive.length>0) {
        [].forEach.call(sizeactive,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        })
    }
    if (sizewomenactive.length>0) {
        [].forEach.call(sizewomenactive,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        })
    }            
if(sizeactive[0]){sizeactive[0].classList.add("active");}
if(sizewomenactive[0]){sizewomenactive[0].classList.add("active");}
if(btnring){
btnring.classList.remove("d-none");
btnfinalcart.classList.add("d-none");
}
if(btnwomenring){
btnwomenring.classList.remove("d-none");
btnwomenfinalcart.classList.add("d-none");
}
if(id){id.classList.add("d-none");}
if(btn){btn.classList.add("d-none");}
if(btncart){btncart.classList.remove("d-none");}
if(idwomen){idwomen.classList.add("d-none");}
});
$(document).on("click",".stack-builder-close-overlay-women", function(){
    men_size_selected = "";
    women_size_selected = "";
    var id = document.getElementById("stack-builder-popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("stack-builder-product-btn-" + this.getAttribute("data-id"));
    var btncart = document.getElementById("stack-builder-btn-product-cart-id-" + this.getAttribute("data-id"));
    var idwomen = document.getElementById("stack-builder-popup-size-women-" + this.getAttribute("data-id"));
    var btnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if (sizeactive.length>0) {
        [].forEach.call(sizeactive,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        })
    }
    if (sizewomenactive.length>0) {
        [].forEach.call(sizewomenactive,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        })
    }            
if(sizeactive[0]){sizeactive[0].classList.add("active");}
if(sizewomenactive[0]){sizewomenactive[0].classList.add("active");}
if(btnring){
btnring.classList.remove("d-none");
btnfinalcart.classList.add("d-none");
}
if(btnwomenring){
btnwomenring.classList.remove("d-none");
btnwomenfinalcart.classList.add("d-none");
}
if(id){id.classList.add("d-none");}
if(btn){btn.classList.add("d-none");}
if(btncart){btncart.classList.remove("d-none");}
if(idwomen){idwomen.classList.add("d-none");}
});
$(document).on("click",".close-overlay-women", function(){
    men_size_selected = "";
    women_size_selected = "";
    var id = document.getElementById("popup-size-" + this.getAttribute("data-id"));
    var btn = document.getElementById("product-btn-" + this.getAttribute("data-id"));
    var btncart = document.getElementById("btn-product-cart-id-" + this.getAttribute("data-id"));
    var idwomen = document.getElementById("popup-size-women-" + this.getAttribute("data-id"));
    var btnring = document.getElementById("buyringsize-" + this.getAttribute("data-id"));
    var btnwomenring = document.getElementById("buyringsize-women-" + this.getAttribute("data-id"));
    var btnfinalcart = document.getElementById("finaladdtocart-" + this.getAttribute("data-id"));
    var btnwomenfinalcart = document.getElementById("finaladdtocart-women-" + this.getAttribute("data-id"));
    var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
    var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
    if (sizeactive.length>0) {
        [].forEach.call(sizeactive,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        })
    }
    if (sizewomenactive.length>0) {
        [].forEach.call(sizewomenactive,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        })
    }            
if(sizeactive[0]){sizeactive[0].classList.add("active");}
if(sizewomenactive[0]){sizewomenactive[0].classList.add("active");}
if(btnring){
btnring.classList.remove("d-none");
btnfinalcart.classList.add("d-none");
}
if(btnwomenring){
btnwomenring.classList.remove("d-none");
btnwomenfinalcart.classList.add("d-none");
}
if(id){id.classList.add("d-none");}
if(btn){btn.classList.add("d-none");}
if(btncart){btncart.classList.remove("d-none");}
if(idwomen){idwomen.classList.add("d-none");}
});
$(document).on("click",".product-options__calc_value", function(){
    var id = document.getElementById("product-size-label-" + this.getAttribute("data-id"));
    try {
    id.innerHTML = "Size " + this.innerHTML;
    } 
    catch(error){
    return;
    }
});
$(document).on("change",".select__dropdown_page", function(){
    var id = document.getElementById("product-size-label-" + this.getAttribute("data-id"));
    var str_val = this.value;
    id.innerHTML = "Size " + str_val.replace('Size','');
});
$(document).on("click",".product-options__value--circle--product-page", function(){
    page_selected_color = this.getAttribute("data-value");
    var actsize = document.querySelectorAll(".product-options__value--size--"  + this.getAttribute("data-id"));
    var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
    var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
    var id = document.getElementById("product-color-label-" + this.getAttribute("data-id"));
    var color = this.getAttribute("data-value");
    var available;
    if(this.classList.contains("disabled")) {
        available = false;
        if(btnpage){btnpage.classList.remove("d-none");}
        if(btnpagecart){btnpagecart.classList.add("d-none");}
    } else {
        available = true;
    }
    if(page_selected_size !== "" && available){
        if(btnpage){btnpage.classList.add("d-none");}
        if(btnpagecart){btnpagecart.classList.remove("d-none");}
    }    
    color = color.match(/[A-Z][a-z]+|[0-9]+/g).join(" ");
    id.innerHTML = "Color " + color; 
        this.classList.add("active");
    if(page_selected_size === "") {
        if(btnpage){btnpage.classList.add("half_disabled_add_to_cart");}
        [].forEach.call(actsize,function(el){
            el.setAttribute("data-js-option-value","");
        }); 
        remove_active_size(actsize,1);        
    }    
});
$(document).on("click",".product-options__value--text--page--size", function(){
    var id = document.getElementById("product-size-label-" + this.getAttribute("data-id"));
    var label = document.getElementById("sizedroplabel-"  + this.getAttribute("data-id"));
    var sizesv = document.getElementById("selected_size_" + this.getAttribute("data-id"));
    var actsize = document.querySelectorAll(".product-options__value--size--"  + this.getAttribute("data-id"));
    page_selected_size = this.getAttribute("data-value");
    var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
    var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
    var available;
    if(this.classList.contains("disabled")) {
        available = false;
        if(btnpage){btnpage.classList.remove("d-none");}
        if(btnpagecart){btnpagecart.classList.add("d-none");}
    } else {
        available = true;
    }
    if(id){
        id.innerHTML = "Size " + this.innerHTML;         
    }
    if(sizesv){sizesv.innerHTML = "Size " + this.innerHTML;}
    if(label){label.innerHTML = "Size " + this.innerHTML;}
    if(page_selected_size !== "" && available){
        btnpage.classList.add("d-none");
        btnpagecart.classList.remove("d-none");
    }    
        [].forEach.call(actsize,function(el){
           el.setAttribute("data-js-option-value","");
        }); 
        this.classList.add("active");
});
function remove_active_size(actsize,nooftime) { 
    var cleartime = setInterval(() => {
        [].forEach.call(actsize,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        }); 
        if (nooftime > 5) {
            clearInterval(cleartime);
        }else{
            nooftime++;
        }    
    }, 100);    
};
function remove_active_color_add_active_size(actsize,selected,actcolor,nooftime) { 
    var cleartime = setInterval(() => {
        [].forEach.call(actsize,function(el){
            if(el.getAttribute("data-value") === selected){
                el.classList.add("active");
            }else{
                el.classList.remove("active");
            }
        }); 
        [].forEach.call(actcolor,function(el){
            if(el.classList.contains("active")){
                el.classList.remove("active");
            }
        }); 
        if (nooftime > 4) {
            clearInterval(cleartime);
        }else{
            nooftime++;
        }    
    }, 200);    
};
$(document).on("click",".product-options__value--circle", function(){
    btn = document.getElementById("btn-product-cart-id-" + this.getAttribute("data-id"));
    sbbtn = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
    if(btn){
    btn.setAttribute("data-selected-color",this.getAttribute("data-value"));
    }   
    if(sbbtn){
    sbbtn.setAttribute("data-color",this.style.backgroundImage);
    }   
});
$(document).on("click",".option-women-size", function(){
var menid = document.getElementById("popup-size-" + this.getAttribute("data-id"));
var womenid = document.getElementById("popup-size-women-" + this.getAttribute("data-id"));
var usmenid = document.getElementById("upsell-popup-size-" + this.getAttribute("data-id"));
var uswomenid = document.getElementById("upsell-popup-size-women-" + this.getAttribute("data-id"));
var act = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
var actmen = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
if(womenid){womenid.classList.remove("d-none");}
if(menid){menid.classList.add("d-none");}
if(uswomenid){uswomenid.classList.remove("d-none");}
if(usmenid){usmenid.classList.add("d-none");}
[].forEach.call(act, function(el){
    if(women_size_selected == ""){
        el.classList.remove("active");
    } 
});
if(men_size_selected == "" && actmen.length>0){
    actmen[0].classList.add("active");
    actmen[0].style.backgroundColor = "white", "important";
}else{
    if(actmen>0){actmen[0].removeAttribute("style");}
};
});
$(document).on("click",".option-men-size", function(){
var menid = document.getElementById("popup-size-" + this.getAttribute("data-id"));
var womenid = document.getElementById("popup-size-women-" + this.getAttribute("data-id"));
var usmenid = document.getElementById("upsell-popup-size-" + this.getAttribute("data-id"));
var uswomenid = document.getElementById("upsell-popup-size-women-" + this.getAttribute("data-id"));
var act = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
var actwomen = document.querySelectorAll(".ring-size-women-active" + this.getAttribute("data-id"));
if(menid){menid.classList.remove("d-none");}
if(womenid){womenid.classList.add("d-none");}
if(usmenid){usmenid.classList.remove("d-none");}
if(uswomenid){uswomenid.classList.add("d-none");}
[].forEach.call(act, function(el){
    if(el.classList.contains("active") && men_size_selected == ""){
        el.classList.remove("active");
    }
});

if(women_size_selected == "" && actwomen.length>0){
    actwomen[0].classList.add("active");
    actwomen[0].style.backgroundColor = "white", "important";
}else{
    if(actwomen>0){actwomen[0].removeAttribute("style");}
};
});
$(document).on("click",".product-options__value--text", function(){
var id = document.getElementById("product-size-label-" + this.getAttribute("data-id"));
var label = document.getElementById("sizedroplabel-"  + this.getAttribute("data-id"));
var size = document.getElementById("selected_size_men_" + this.getAttribute("data-id"));
var sizew = document.getElementById("selected_size_women_" + this.getAttribute("data-id"));
var sizesv = document.getElementById("selected_size_" + this.getAttribute("data-id"));
var btnring = document.getElementById("buyringsize-" + this.getAttribute("data-id"));
var btnwomenring = document.getElementById("buyringsize-women-" + this.getAttribute("data-id"));
var btncart = document.getElementById("finaladdtocart-" + this.getAttribute("data-id"));
var btnwomencart = document.getElementById("finaladdtocart-women-" + this.getAttribute("data-id"));
var usid = document.getElementById("upsell-product-size-label-" + this.getAttribute("data-id"));
var uslabel = document.getElementById("upsell-sizedroplabel-"  + this.getAttribute("data-id"));
var ussizesv = document.getElementById("upsell-selected_size_" + this.getAttribute("data-id"));
var usbtnring = document.getElementById("upsell-buyringsize-" + this.getAttribute("data-id"));
var usbtnwomenring = document.getElementById("upsell-buyringsize-women-" + this.getAttribute("data-id"));
var usbtncart = document.getElementById("upsell-finaladdtocart-" + this.getAttribute("data-id"));
var usbtnwomencart = document.getElementById("upsell-finaladdtocart-women-" + this.getAttribute("data-id"));
var sbid = document.getElementById("stack-builder-product-size-label-" + this.getAttribute("data-id"));
var sblabel = document.getElementById("stack-builder-sizedroplabel-"  + this.getAttribute("data-id"));
var sbsizesv = document.getElementById("stack-builder-selected_size_" + this.getAttribute("data-id"));
var sbbtnring = document.getElementById("stack-builder-buyringsize-" + this.getAttribute("data-id"));
var sbbtnwomenring = document.getElementById("stack-builder-buyringsize-women-" + this.getAttribute("data-id"));
var sbbtncart = document.getElementById("stack-builder-finaladdtocart-" + this.getAttribute("data-id"));
var sbbtnwomencart = document.getElementById("stack-builder-finaladdtocart-women-" + this.getAttribute("data-id"));
var act = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
var actmen = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
men_size_selected = this.getAttribute("data-value");
page_selected_size1 = this.getAttribute("data-value");
var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
var usbtnpage = document.getElementById("usbuycartpage-" + this.getAttribute("data-id"));
var usbtnpagecart = document.getElementById("usaddcartpage-" + this.getAttribute("data-id"));
var sbbtnpage = document.getElementById("stack-builder-buycartpage-" + this.getAttribute("data-id"));
var sbbtnpagecart = document.getElementById("stack-builder-addcartpage-" + this.getAttribute("data-id"));
var available;
if(this.classList.contains("disabled")) {
    available = false;
    if(btnring){btnring.classList.remove("d-none");}
    if(btncart){btncart.classList.add("d-none");}
    if(usbtnring){usbtnring.classList.remove("d-none");}
    if(usbtncart){usbtncart.classList.add("d-none");}
    if(sbbtnring){sbbtnring.classList.remove("d-none");}
    if(sbbtncart){sbbtncart.classList.add("d-none");}
    if(btnwomenring){btnwomenring.classList.remove("d-none");}
    if(btnwomencart){btnwomencart.classList.add("d-none");}
    if(usbtnwomenring){usbtnwomenring.classList.remove("d-none");}
    if(usbtnwomencart){usbtnwomencart.classList.add("d-none");}
    if(sbbtnwomenring){sbbtnwomenring.classList.remove("d-none");}
    if(sbsbtnwomencart){sbbtnwomencart.classList.add("d-none");}
} else {
    available = true;
}
if(id){id.innerHTML = "Size " + this.innerHTML;}
if(usid){usid.innerHTML = "Size " + this.innerHTML;}
if(sbid){sbid.innerHTML = "Size " + this.innerHTML;}
if(sizesv){sizesv.innerHTML = "Size " + this.innerHTML;}
if(ussizesv){ussizesv.innerHTML = "Size " + this.innerHTML;}
if(sbsizesv){sbsizesv.innerHTML = "Size " + this.innerHTML;}
if(label){label.innerHTML = "Size " + this.innerHTML;}
if(uslabel){uslabel.innerHTML = "Size " + this.innerHTML;}
if(sblabel){sblabel.innerHTML = "Size " + this.innerHTML;}
if(btncart && btncart.classList.contains("d-none") && act.length==0 && available){
    if(btnring){btnring.classList.add("d-none");}
    if(btncart){btncart.classList.remove("d-none");}
}
if(usbtncart && usbtncart.classList.contains("d-none") && act.length==0 && available){
    if(usbtnring){usbtnring.classList.add("d-none");}
    if(usbtncart){usbtncart.classList.remove("d-none");}
}
if(sbbtncart && sbbtncart.classList.contains("d-none") && act.length==0 && available){
    if(sbbtnring){sbbtnring.classList.add("d-none");}
    if(sbbtncart){
        sbbtncart.classList.remove("d-none");
        sbbtncart.setAttribute("data-size", this.getAttribute("data-value"));
    }
}
if(btncart && btncart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
    if(btnring){btnring.classList.add("d-none");}
    if(btncart){btncart.classList.remove("d-none");}
}
if(usbtncart && usbtncart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
    if(usbtnring){usbtnring.classList.add("d-none");}
    if(usbtncart){usbtncart.classList.remove("d-none");}
}
if(sbbtncart && sbbtncart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
    if(sbbtnring){sbbtnring.classList.add("d-none");}
    if(sbbtncart){sbbtncart.classList.remove("d-none");}
}
if(btnwomencart && btnwomencart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
    if(btnwomenring){btnwomenring.classList.add("d-none");}
    if(btnwomencart){btnwomencart.classList.remove("d-none");}
}
if(usbtnwomencart && usbtnwomencart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
    if(usbtnwomenring){usbtnwomenring.classList.add("d-none");}
    if(usbtnwomencart){usbtnwomencart.classList.remove("d-none");}
}
if(sbbtnwomencart && sbbtnwomencart.classList.contains("d-none") && men_size_selected !== "" && women_size_selected !== "" && available){
    if(sbbtnwomenring){sbbtnwomenring.classList.add("d-none");}
    if(sbbtnwomencart){sbbtnwomencart.classList.remove("d-none");}
}
if(women_size_selected === ""){
    if(btnpage) {btnpage.innerHTML = "Add to cart";}
    if(usbtnpage) {usbtnpage.innerHTML = "Add to cart";}
    if(act.length>0){act[0].style.backgroundColor = "white", "important";}
}else{
    if(act.length>0){act[0].removeAttribute("style");}
}
if(actmen.length>0){
    actmen[0].removeAttribute("style");
}
if(size && men_size_selected) {
    size.innerHTML = "Size " + men_size_selected.replace("Size","") + "/";
    sizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/";
}
if(sizew && women_size_selected && men_size_selected) {
    size.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
    sizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
}try {
    if(page_selected_size1 !== "" && page_selected_size2 !== ""){
        if(bnpage && btnpage.classList.contains("d-none") == false) {
            btnpage.classList.add("d-none");
            if(btnpagecart){btnpagecart.classList.remove("d-none");}
          }       
          if(usbtnpage && usbtnpage.classList.contains("d-none") == false) {
            usbtnpage.classList.add("d-none");
            if(usbtnpagecart){usbtnpagecart.classList.remove("d-none");}
          }       
    } 
} catch(err){ return;}  
[].forEach.call(act, function(el){
    if(women_size_selected == ""){
        el.classList.remove("active");
    } 
});
});
$(document).on("click",".product-options__value--text--page--men", function(){
        var size = document.getElementById("product-size-label-" + this.getAttribute("data-id"));
        var label = document.getElementById("sizedroplabel-" + this.getAttribute("data-id"));
        var actwomen = document.querySelectorAll(".women_variants_size_active_" + this.getAttribute("data-id"));
        var actmen = document.querySelectorAll(".men_variants_size_active_" + this.getAttribute("data-id"));
        var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
        var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
        men_size_selected = this.getAttribute("data-value");
        var available;
        if(this.classList.contains("disabled")) {
            available = false;
        } else {
            available = true;
        }
        if(size){size.innerHTML = "Size " + this.innerHTML;}
        if(label){label.innerHTML = "Size " + this.innerHTML;}
        if(btnpage && men_size_selected !== "" && women_size_selected !== "" && available){
            if(actwomen){actwomen[0].removeAttribute("style");}
            if(actmen){actmen[0].removeAttribute("style");}
            btnpage.classList.add("d-none");
            btnpagecart.classList.remove("d-none");
        }
        if(women_size_selected === ""){           
            if(btnpage) {btnpage.classList.add("half_disabled_add_to_cart");}
            actwomen[0].style.backgroundColor = "white", "important";
            remove_active_size(actwomen,1);
        }
        [].forEach.call(actmen,function(el){
            el.setAttribute("data-js-option-value","");
        });
        this.classList.add("active");
        if(size){size.innerHTML = "Size " + men_size_selected.replace("Size","");}
  });
    $(document).on("click",".product-options__value--text--page--women", function(){
        var size = document.getElementById("product-size-label-women-" + this.getAttribute("data-id"));
        var label = document.getElementById("sizedroplabelwomen-" + this.getAttribute("data-id"));
        var actmen = document.querySelectorAll(".men_variants_size_active_" + this.getAttribute("data-id"));
        var actwomen = document.querySelectorAll(".women_variants_size_active_" + this.getAttribute("data-id"));
        var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
        var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
        women_size_selected = this.getAttribute("data-value");        
        if(size){size.innerHTML = "Size " + this.innerHTML;}
        if(label){label.innerHTML = "Size " + this.innerHTML;}
        if(btnpage && men_size_selected !== "" && women_size_selected !== ""){
            btnpage.classList.add("d-none");
            btnpagecart.classList.remove("d-none");
        }       
        if(actwomen.length>0){
            actwomen[0].removeAttribute("style");
            [].forEach.call(actwomen,function(el){
                if(el.classList.contains("active")) {
                    el.classList.remove("active");
                }
                el.setAttribute("data-js-option-value","");
            });
        }
        if(men_size_selected === ""){
            if(btnpage) {btnpage.classList.add("half_disabled_add_to_cart");}
            [].forEach.call(actmen,function(el){
                if(el.classList.contains("active")) {
                    el.classList.remove("active");
                }
                el.setAttribute("data-js-option-value","");
            });
            if(actmen.length>0){
                actmen[0].style.backgroundColor = "white", "important";
            }
        }
        [].forEach.call(actwomen,function(el){
            el.setAttribute("data-js-option-value","");
        });
        this.classList.add("active");
        if(size){size.innerHTML = "Size " + women_size_selected.replace("Size","");}
        });
$(document).on("click",".product-options__value--text--women", function(){
var id = document.getElementById("product-size-label-women-" + this.getAttribute("data-id"));
var label = document.getElementById("sizedroplabelwomen-" + this.getAttribute("data-id"));
var btnring = document.getElementById("buyringsize-" + this.getAttribute("data-id"));
var btnwomenring = document.getElementById("buyringsize-women-" + this.getAttribute("data-id"));
var btncart = document.getElementById("finaladdtocart-" + this.getAttribute("data-id"));
var btnwomencart = document.getElementById("finaladdtocart-women-" + this.getAttribute("data-id"));
var usid = document.getElementById("upsell-product-size-label-women-" + this.getAttribute("data-id"));
var uslabel = document.getElementById("upsell-sizedroplabelwomen-" + this.getAttribute("data-id"));
var usbtnring = document.getElementById("upsell-buyringsize-" + this.getAttribute("data-id"));
var usbtnwomenring = document.getElementById("upsell-buyringsize-women-" + this.getAttribute("data-id"));
var usbtncart = document.getElementById("upsell-finaladdtocart-" + this.getAttribute("data-id"));
var usbtnwomencart = document.getElementById("upsell-finaladdtocart-women-" + this.getAttribute("data-id"));
var sizewomenactive = document.querySelectorAll(".ring-size-women-active-" + this.getAttribute("data-id"));
var sizeactive = document.querySelectorAll(".ring-size-active-" + this.getAttribute("data-id"));
var sizew = document.getElementById("selected_size_women_" + this.getAttribute("data-id"));
var size = document.getElementById("selected_size_men_" + this.getAttribute("data-id"));
var ussizew = document.getElementById("upsell-selected_size_women_" + this.getAttribute("data-id"));
var ussize = document.getElementById("upsell-selected_size_men_" + this.getAttribute("data-id"));
page_selected_size2 = this.getAttribute("data-value");
var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
var usbtnpage = document.getElementById("upsell-buycartpage-" + this.getAttribute("data-id"));
var usbtnpagecart = document.getElementById("upsell-addcartpage-" + this.getAttribute("data-id"));
women_size_selected = this.getAttribute("data-value");
if (sizewomenactive.length>0) {
    [].forEach.call(sizewomenactive,function(el){
            el.classList.remove("active");
    })
};
if(sizewomenactive.length>0){sizewomenactive[0].removeAttribute("style");}
if(men_size_selected === ""){
    if(btnpage) {
        btnpage.innerHTML = "Add to cart";
        btnpage.classList.add("half_disabled_add_to_cart");
    }
    if(usbtnpage) {
        usbtnpage.innerHTML = "Add to cart";
        usbtnpage.classList.add("half_disabled_add_to_cart");
    }
    if(sizeactive.length>0){
        sizeactive[0].style.backgroundColor = "white", "important";
    }
};
this.classList.add("active");
if(id){id.innerHTML = "Size " + this.innerHTML;}
if(usid){id.innerHTML = "Size " + this.innerHTML;}
if(label){label.innerHTML = "Size " + this.innerHTML;}
if(uslabel){uslabel.innerHTML = "Size " + this.innerHTML;}
if(btncart && btncart.classList.contains("d-none") && men_size_selected !== ""){
    btnring.classList.toggle("d-none");
    btncart.classList.toggle("d-none");
}
if(usbtncart && usbtncart.classList.contains("d-none") && men_size_selected !== ""){
    usbtnring.classList.toggle("d-none");
    usbtncart.classList.toggle("d-none");
}
if(btnwomencart && btnwomencart.classList.contains("d-none") && men_size_selected !== ""){
    btnwomenring.classList.toggle("d-none");
    btnwomencart.classList.toggle("d-none");
}
if(usbtnwomencart && usbtnwomencart.classList.contains("d-none") && men_size_selected !== ""){
    usbtnwomenring.classList.toggle("d-none");
    usbtnwomencart.classList.toggle("d-none");
}
if(sizew && women_size_selected) {
    sizew.innerHTML = "Size " + "/" + women_size_selected.replace("Size","");
    size.innerHTML = "Size " + "/" + women_size_selected.replace("Size","");
}
if(sizew && men_size_selected && women_size_selected) {
    sizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
    size.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");
}
if(ussize){ussize.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");ussizew.innerHTML = "Size " + men_size_selected.replace("Size","") + "/" + women_size_selected.replace("Size","");}
try {
    if(page_selected_size1 !== "" && page_selected_size2 !== ""){
        if(btnpage && btnpage.classList.contains("d-none") == false) {
            btnpage.classList.toggle("d-none");
            btnpagecart.classList.toggle("d-none");
          }     
          if(usbtnpage && usbtnpage.classList.contains("d-none") == false) {
            usbtnpage.classList.toggle("d-none");
            usbtnpagecart.classList.toggle("d-none");
          }    
    } 
} catch(err){ return;}  
});
$(document).on("click",".product-options__value--large-text", function(){
var id = document.getElementById("product-size-label-" + this.getAttribute("data-id"));
var label = document.getElementById("sizedroplabel-"  + this.getAttribute("data-id"));
var btnring = document.getElementById("buyringsize-" + this.getAttribute("data-id"));
var btnwomenring = document.getElementById("buyringsize-women-" + this.getAttribute("data-id"));
var btncart = document.getElementById("finaladdtocart-" + this.getAttribute("data-id"));
var btnwomencart = document.getElementById("finaladdtocart-women-" + this.getAttribute("data-id"));
var size = document.getElementById("selected_size_" + this.getAttribute("data-id"));
page_selected_size = this.getAttribute("data-value");
var btnpage = document.getElementById("buycartpage-" + this.getAttribute("data-id"));
var btnpagecart = document.getElementById("addcartpage-" + this.getAttribute("data-id"));
   if(id){
        id.innerHTML = "Size " + this.innerHTML;
        label.innerHTML = "Size " + this.innerHTML;
    }
    if(btncart && btncart.classList.contains("d-none")){
        btnring.classList.toggle("d-none");
        btncart.classList.toggle("d-none");
    }
    if(btnwomencart && btnwomencart.classList.contains("d-none")){
        btnwomenring.classList.toggle("d-none");
        btnwomencart.classList.toggle("d-none");
    }
    if(size){
        size.innerHTML = "Size " + this.innerHTML;
    }
    try {
    if(page_selected_color !== "" && page_selected_size !== ""){
        if(btnpage && btnpage.classList.contains("d-none") == false) {
            btnpage.classList.toggle("d-none");
            btnpagecart.classList.toggle("d-none");
          }       
    } 
} catch(err){ return;}  
});
$(document).on("click",".sizedropbtn", function(){
el = this.nextElementSibling;
el.classList.toggle("sizeshow");
var actmen = document.querySelectorAll(".men_variants_size_active_" + this.getAttribute("data-id"));
var actwomen = document.querySelectorAll(".women_variants_size_active_" + this.getAttribute("data-id"));
if(women_size_selected === ""){
    if(actwomen.length>0){
        actwomen[0].classList.remove("active");
    }
    if(actmen.length>0 && men_size_selected !== ""){
    [].forEach.call(actmen,function(el){
        if(el.getAttribute("data-value") == men_size_selected ){
            el.classList.add("active");
        }
    });
    }
}
if(men_size_selected === ""){
    if(actmen.length>0){
        actmen[0].classList.remove("active");
    }
    if(actwomen.length>0 && women_size_selected !== ""){
        [].forEach.call(actwomen,function(el){
            if(el.getAttribute("data-value") == women_size_selected ){
                el.classList.add("active");
            }
        });
    }
}
});
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
if (!event.target.matches('.sizedropbtn')) {
var dropdowns = document.getElementsByClassName("sizedropdown-content");
  for (i = 0; i < dropdowns.length; i++) {
    var openDropdown = dropdowns[i];
    if (openDropdown.classList.contains('sizeshow')) {
        openDropdown.classList.remove('sizeshow');
      }
    }
  }
};
